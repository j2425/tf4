data "aws_vpc" "default" {
  default = true
}

variable "tf55_traffic_rules" {
  type          = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 80 
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 8000
      to_port     = 8000
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 8802
      to_port     = 8802
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

variable "ebs_volumes" {
  description         = "Disks for attachment"
  type                = map(object({
    size              = number
    availability_zone = string
    device_name       = string
    tag               = map(string)
    instance          = number
  }))
  default = {
    disk1 = {
      size              = 10
      availability_zone = "eu-central-1a"
      device_name       = "sdb"
      tag               = {
        Name = "tf55_disk1"
      }
      instance = 0
    }
    disk2 = {
      size              = 8
      availability_zone = "eu-central-1a"
      device_name       = "sdc"
      tag               = {
        Name = "tf55_disk2"
      }
      instance = 0
    }
    disk3 = {
      size              = 8
      availability_zone = "eu-central-1b"
      device_name       = "sdb"
      tag               = {
        Name = "tf55_disk3"
      }
      instance = 1
    }
    disk4 = {
      size              = 8
      availability_zone = "eu-central-1b"
      device_name       = "sdc"
      tag               = {
        Name = "tf55_disk4"
      }
      instance = 1
    }
  }
}

resource "aws_security_group" "tf55_security_group" {
  name        = "tf55_sg"
  description = "Allow 80 8000 8802"

  dynamic "ingress" {
    for_each     = var.tf55_traffic_rules
    content {
     from_port   = ingress.value.from_port
     to_port     = ingress.value.to_port
     protocol    = ingress.value.protocol
     cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "tf55_sg"
  }
}

resource "aws_ebs_volume" "ebs_volumes" {
  for_each = var.ebs_volumes

  availability_zone = each.value.availability_zone
  size              = each.value.size
  tags              = each.value.tag
}

resource "aws_instance" "app_server" {
  count                = 2
  ami                  = var.ami_id
  instance_type        = "t2.micro"
  availability_zone    = element(["eu-central-1a", "eu-central-1b"], count.index)
  iam_instance_profile = "DemoRoleforEC2"
  key_name             = "tutorial_key"
  security_groups      = ["tf55_sg"]
  root_block_device {
    volume_size = "20"
  }

  tags = {
    Name      = "eugesterh-${count.index + 1}" 
    blog-dev  = "true"
  }

  provisioner "local-exec" {
    command = "echo '${self.public_dns} ${self.public_ip}' >> hosts.list"
  }

}

resource "aws_volume_attachment" "volume_attachments" {
  for_each    = var.ebs_volumes

  device_name = "/dev/${each.value.device_name}"
  volume_id   = aws_ebs_volume.ebs_volumes[each.key].id
  instance_id = aws_instance.app_server[each.value.instance].id
}

resource "aws_lb" "load_balancer" {
  name               = "blog-load-balancer"
  internal           = false
  load_balancer_type = "application"

  security_groups = [aws_security_group.tf55_security_group.id]

  subnets = tolist([
    for instance in aws_instance.app_server :
    instance.subnet_id
  ])

  tags = {
    Name = "blog-load-balancer"
  }
}

resource "aws_lb_target_group" "target_group" {
  name     = "blog-target-group"
  port     = 8000
  protocol = "HTTP"

  vpc_id = data.aws_vpc.default.id

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}

resource "aws_lb_target_group_attachment" "target_group_attachment" {
  count            = 2
  target_group_arn = aws_lb_target_group.target_group.arn
  target_id        = aws_instance.app_server[count.index].id
  port             = 8000
}

resource "null_resource" "delay" {
  provisioner "local-exec" {
    command = "sleep 30"  
  }

  depends_on = [aws_instance.app_server]
}
resource "null_resource" "ssh_keyscan" {
  count = length(aws_instance.app_server)

  provisioner "local-exec" {
    command = "ssh-keyscan ${aws_instance.app_server[count.index].public_dns} >> ~/.ssh/known_hosts"
  }
  depends_on = [null_resource.delay]
}

resource "null_resource" "dependency" {
  triggers = {
    instance_ids = join(",", aws_instance.app_server.*.id)
  }
  
  provisioner "local-exec" {
    command = <<-EOT
    ansible-playbook -i inventory/aws_ec2.yml playblog.yml \
    -e 'private_key_path=${var.private_key_path}' \
    -e 'instance_user=${var.instance_user}'
    EOT
    working_dir = "playbook/"
  }

  depends_on = [null_resource.ssh_keyscan]
}