variable "private_key_path" {
    description = "Path to the ssh key file for ec2 access"
}

variable "ami_id" {
    description = "Ami ID for instances"
    default = "ami-0b0c5a84b89c4bf99"
}

variable "instance_user" {
    description = "Admin user for instances"
}