output "instances" {
  description = "instance_id, instance_public_ip, instance_name"
  value = [    for idx, inst_id in aws_instance.app_server[*].id :
    {
      instance_id = inst_id
      instance_name = aws_instance.app_server[idx].public_dns
      public_ip = aws_instance.app_server[idx].public_ip
    }
  ]
}

output "lb_dns" {
  description = "DNS name of ELB"
  value = aws_lb.load_balancer.dns_name
}
